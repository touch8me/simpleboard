# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-22 07:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('board', '0008_auto_20160922_0315'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reply',
            old_name='reply',
            new_name='comment',
        ),
        migrations.AlterField(
            model_name='reply',
            name='parent',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='board.Reply'),
        ),
    ]
